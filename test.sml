
structure Lambda = 
struct 
   (* First import  the list-based set implementation to be used *)
   structure MSet :> Set = ListSet
   type 'a Set  = 'a MSet.Set
   (* Define a datatype for representing lambda terms as data-structures (trees!) *)
   datatype expr = 
	    var of string | 
	    apply of expr * expr |
	    abs of  expr * expr

   (* An exception to report an error when something terribly bad happens *)
   exception Bad of string
	
   (* toString is a convenience function to convert a lambda expression tree
      into a string that can be printed (useful for debugging!)
      *)	    
   fun toString x =
		case x
			of   var x => x
				| apply (x,y) => toString(x) ^ toString(y)
				| abs (x,y) => toString(x) ^ toString(y);
				
				
end 