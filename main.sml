(* This is where you place your main program that uses the lambda
   structure and the church structure to test the interpreter on 
   the computation of the fibonacci number. 
*)
use "expr.sml";

Main.fib();

