use "set-sig.sml";
use "set.sml";

structure Lambda = 
struct 
   (* First import  the list-based set implementation to be used *)
   structure MSet : Set = ListSet
   type 'a Set  = 'a MSet.Set
   (* Define a datatype for representing lambda terms as data-structures (trees!) *)
   datatype expr = 
	    var of string | 
	    apply of expr * expr |
	    abs of  expr * expr

   (* An exception to report an error when something terribly bad happens *)
   exception Bad of string
	
   (* toString is a convenience function to convert a lambda expression tree
      into a string that can be printed (useful for debugging!)
      *)	    
   fun toString x =
		case x
			of   var x => x
				| apply 	( x,y ) => "( " ^ toString(x) ^ toString(y) ^ " )"
				| abs	( x,y ) => "( \\" ^ toString(x) ^ "." ^ toString(y) ^ " )";
		

   (* The printing routine simply uses toString to obtain a string and dunp it on the standard output.
      *)
   fun printIt x =  (print(toString(x)); x)
    
    (* freeV computes the set of free variables appearing in the lambda term denoted by x
      *)
   fun freeV x = 
		case x
			of			 ( var x ) => MSet.single x
			| apply	 ( x,y ) => MSet.union (freeV x) (freeV y)
			| abs 	 ( var x,y ) => MSet.remove x (freeV y)
			| _ => raise Bad "Invalid abs() expression";
       
   (* newName is a helper function charged with producing a name based on the "root" string (x) that does not
      belong to the set s. Namely, the output o is such that o \NOTIN s. For instance, if the root string is "foo"
      and the set s = { foo,foo_1,foo_2,foo_3}, a valid output would be foo_4
      *)    
   fun newName x s = 
		let fun rename x = x ^ "_" 
		in
			if MSet.member x s then 
				newName (rename x) s
			else x
		end;
	
   (* subst is meant to carry out a substitution. Namely, it computes [y/x]z, i.e., it replaces every free 
      occurences  of x by y within the lambda term z.
      *)
   fun subst x y z = 
		case z
			of var t => if x = t then y else var t
			| apply	(h, t) =>  apply(subst x y h, subst x y t)
			| abs	(var h, t) => if MSet.member h (freeV y) 
								  then abs(var h,t) 
								  else abs( var h, subst x y t )
			| _ => raise Bad "Invalid Substitution";
       
   (* alpha implements the alpha-equivalence axiom and is responsible for producing a lambda term structurally equivalent
      to e but where any binder must use a variable name _not_ appearing in the set s. For instance, the lambda term
      Given a set s = {x}   the lambda term \x.\y.x y z    should be alpha renamed to \x_0.\y.x_0 y z to prevent any 
      accidental binding resulting from a beta reduction where \x.\y.x y is the body of the function 
      \z.\x.\y.x y z  and the argument is, for instance, x. Indeed, without alpha renaming, applying a direct substitution
      would incorrectly bind x! [x/z](\x.\y.x y z) ->beta  \x.\y.x y x  which is not the intended meaning. A suitable renaming
      of the binder does prevent the erroneous behavior.
      *)
   fun alpha e s = 
		case e
			of var e => if ( MSet.member e s ) then var (newName e s) else var e
			| 	abs		(var h,t) => if MSet.member h s 
										then let 
											val newS = MSet.union (freeV t) s
											val name = (newName h newS) 
											val newBody = (subst h (var name) t)
											in 
												abs( (var name),  (alpha t (newS) ) )
											end
										else abs( var h,t )
			| 	apply	(h,t) => apply(  alpha h s , alpha t s )
			| _ => raise Bad "Improper Lambda Expresion, first expr in abs must match type var";

   (* beta implements the beta reduction discussed in class. Naturally, before reducing an application (A B) one should
      first make sure that no accidental binding can occur and, if they do, make use of the alpha reduction to carry out
      a suitable renaming.
      *)
   fun beta  e =  
		case e
			of apply 	( abs ( var a,b ), p ) => subst a (alpha p (freeV (b))) (alpha b (freeV (p)))
			| 	apply 	( abs ( _, b ), _ ) => raise Bad "Improper Lambda Expresion, first expr in abs must match type var"
			|	_ => raise Bad "Not Reducable"


					(*case p
					of abs( var x, y ) =>
						let val free = freeV ( abs( var x, y ) ) in
							case q
								of abs( var t, z ) => if MSet.member t free then subst x (alpha (abs(var t, z)) free) (y) else	subst x (abs(var t, z)) y
								| var q =>	subst x (var q) (alpha y (freeV (var q)))
								| apply(h,t) => subst x (apply(h,t)) (alpha y (freeV (apply(h,t)))) 
						end
					|  apply( p, q ) => raise Bad "doh"
					|  var p => raise Bad "doh2"*)

				
	;			
		
   (* normal is meant to test whether the input lambda term is or is not in irreducible form. Recall that a lambda term
      is irreducible if and only if it does not contain any application of the form ((\x.M) B)
      *)
   fun normal e = 
		case e
			of apply	( abs( var a,b ), p ) => false
			|	apply	( a,b ) => if normal a andalso normal b then true else false
			|	abs		( var a,b ) => normal b 
			|	abs 		( _,b ) => raise Bad "Incorrect Syntax"
			|	var e => true;
			   
   (* simp is the top-level lambda-calculus reduction function. It takes as input a lambda term e and recursively 
      reduces it until we obtain a normal form. The output is the irreducible answer.
      *)
   fun simp e = 
		case e
			of var e => var e
			|	abs 		( a,b ) => if not (normal b) then  abs( a, (simp b) ) else abs( a,b )
			|   apply 	( abs( a,b ), p ) => simp ( beta ( apply ( abs( a,b ), p ) ) )
			|	apply 	( a,b ) => if not (normal a) then apply( (simp a), b ) else if not (normal b) then abs ( a, (simp b) ) else apply( a,b );
end;
