use "set-sig.sml";
use "set.sml";
use "lambda-sig.sml";
use "lambda.sml";
use "church-sig.sml";
use "church.sml";
         
structure Main = struct     
structure L : LExpr = Lambda;
structure C : ChurchSig = Church;

(* We do not have a parser. To make things easy, I'm providing the definition of all the auxiliary 
   terms to define the recursive fibonacci function with lambda terms. The defitions include all the
   expected things (church numerals for the constants, lambda expressions for arithmetic operations, branching
   Y combinator, etc... Make sure you understand these definitions. Feel free to print them out (with your
   toString function to confirm you understand them.
 *)
fun fib () = 
	let open L in
    let val t = abs(var "x",apply(var "f",apply(var "x",var "x")))    (* \x.f (x x)   *)
	    val Y = abs(var "f",apply(t,t))                               (* \f.\x.f (x x) \x.f (x x)  *)
	    val succ = abs(var "n",abs(var "s",abs(var "z", 
						   apply(var "s",
							 apply(apply(var "n",
								     var "s"),
							       var "z")))))
	    val zero = abs(var "s",abs(var "z",var "z"))                 (* Church numeral 0  *)
	    val tlam = abs(var "x",abs(var "y",var "x"))                 (* true =  \x.\y.x *)
	    val flam = abs(var "x",abs(var "y",var "y"))                 (* false = \x.\y.y *)
	    val isZ  = abs(var "n",apply(apply(var "n",abs(var "x",flam)),tlam))     (* isZero *)
	    val plus = abs(var "n",abs(var "m",apply(apply(var "n",succ),var "m")))  (* addition *)          	    
	    val mult = abs(var "n",abs(var "m",abs(var "f",apply(var "n",apply(var "m",var "f")))))   (* multiplication *)
	    val pair = abs(var "a",abs(var "b",abs(var "z",apply(apply(var "z",var "a"),var "b"))))   (* pairing *)
	    val fst  = abs(var "p",apply(var "p",tlam))
	    val snd  = abs(var "p",apply(var "p",flam))
	    val pp   = abs(var "p",apply(apply(pair,apply(snd,var "p")),apply(succ,apply(snd,var "p"))))
	    val pred = abs(var "n",apply(fst,apply(apply(var "n",pp),apply(apply(pair,zero),zero))))
	    val ifte = abs(var "c",abs(var "t",abs(var "e",apply(apply(var "c",var "t"),var "e"))))    (* if-then-else *)
	    val nm1  = apply(pred,var "n")                                                             (* n - 1 *)
	    val nm2 =  apply(pred,nm1)                                                                 (* n - 2 *)
	    val one = C.int2Church(1)
	    val four = C.int2Church(4)
	    val five = C.int2Church(5)
	    val seven = C.int2Church(7)
	    val fib  = abs(var "f",abs(var "n",
				       apply(apply(apply(ifte,
							 apply(isZ,var "n")),
						   zero),
					     apply(apply(apply(ifte,
							       apply(isZ,nm1)),
							 one),
						   apply(apply(plus,
							       apply(var "f",nm1)),
							 apply(var "f",nm2))))))
	    val fib4 = apply(apply(Y,fib),four)
	    val fib5 = apply(apply(Y,fib),five)
	    val fib7 = apply(apply(Y,fib),seven)

    in printIt (simp fib7)
    end
end
end