structure Church : ChurchSig =
struct
   exception Bad of string
   structure L : LExpr = Lambda
   type expr = L.expr;
   local open L in
   (* A simple function to convert a lambda term representing a natural number into its 'usual' representation *)
   fun church2Int e = 
		case e
			of L.var e => 0
			|  L.apply(x,y) => (church2Int y) + 1 (* implement this *)
			|  L.abs(x,y) => church2Int y + 0
			
   (* A simple function to convert a 'usual' natural number (non-negative) into a church numeral *)
    fun int2Church v =
			let fun addF v =  
				if v > 0 
				then apply( var "f", addF( v -1 )) 
				else var "x"
			in
				if v = 0 then abs( var "f", abs( var "x", var "x") )
				else abs( var "f", abs( var "x",  (addF v) ) )
			end
		
						(* implement this *) 
	(* fun int2Church 0 =  (fn f => fn x => x)
		| int2Church v = fn f => fn x => f (int2Church (v-1) f x);  *) (* implement this *) 
	end
end
